const prompt = require('prompt');
const io = require('socket.io-client');
const input = process.argv; //to read data from command prompt
prompt.start();
let myTurn = true
let playerSymbol = 'X'
let playerNumber = "first"
let socketId
let boardData


let url = `http://${input[2]}:${input[3]}`
let socket = io.connect(url); // client establish connection to server
socket.on('connect', () => {
  socketId = socket.id //socket id for future reference
  console.log("Player Id: " + socketId); 
});


/* 
function used to display the current board status
*/
const printBoard = async (board) => {
  console.log("Current Board")
  console.log('\n' +
    ' ' + board[1] + ' | ' + board[2] + ' | ' + board[3] + '\n' +
    ' ---------\n' +
    ' ' + board[4] + ' | ' + board[5] + ' | ' + board[6] + '\n' +
    ' ---------\n' +
    ' ' + board[7] + ' | ' + board[8] + ' | ' + board[9] + '\n');
}


let winCombinations = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [1, 4, 7], [2, 5, 8], [3, 6, 9], [1, 5, 9], [3, 5, 7]];  //winning possible combinations

/* 
Validate user move
*/
const validateMove = async (position, board) => {
  return (!isNaN(position) && board[position] === ' ')
}

/* 
logic to check is mach is tie 
*/
const checkTie = async (board) => {
  for (const key in board) {
    if (board[key] === ' ') {
      return false;
    }
  }
  return true;
}

/* 
logic to check is match is won
*/
const checkWin = async (board) => {
  for (const position of winCombinations) {
    if ((board[position[0]] === 'X' || board[position[0]] === 'O') && (board[position[1]] === 'X' || board[position[1]] === 'O') && (board[position[2]] === 'X' || board[position[2]] === 'O')) {
      if (board[position[0]] === board[position[1]] && board[position[0]] === board[position[2]]) {
        return true
      }
    }
  }
  return false
}

/* 
below function used to display instruction
*/
const instruction = async () => {
  console.log("Welcome to tic tac toe")
  console.log(`Below are the instructions`)
  console.log('The Tic-Tac-Toe board is numbered like this:')
  for (let i = 1; i < 10; i++) {
    console.log(i)
  }
  console.log("Please enter between 1-9 to hold the position as per displayed below")
  console.log(' 1 | 2 | 3 \n' +
    ' --------- \n' +
    ' 4 | 5 | 6 \n' +
    ' --------- \n' +
    ' 7 | 8 | 9 \n');

  console.log(`To quit game enter: "r" \n`)
}


/* 
below function used to get command line arguments from users using prompt
*/
const getInput = async (data) => {
  console.log(`It's Your turn, So please make a move`);
  prompt.get(['position'], async function (err, result) {
    //condition used to check whether client quit the game
    if (result.position === 'r' || result.position === 'R') { 
      //emit result to server
      socket.emit("client_result", {
        id: data._id,
        result: 'won',
        winnerPlayer: playerNumber == "first" ? "second" : "first"
      });
      return;
    }
    let validation = await validateMove(result.position, data.board) // initializing function to validate move
    if (validation) {
      let position = result.position
      myTurn = false
      let playerOneTurn = playerNumber == "first" ? false : true // used to check player turn after move_made
      let payload = {
        board:
        {
          [position]: playerSymbol
        },
        playerOneTurn: playerOneTurn,
        playerSymbol: playerSymbol,
        _id: data._id
      }
      boardData = data.board
      boardData[position] = playerSymbol
      let tie = await checkTie(boardData) // initializing function to check tie
      let win = await checkWin(boardData)  // initializing function to check win
      if (win) {
        //emit win result to server
        socket.emit("client_result", {
          id: data._id,
          result: 'won',
          winnerPlayer: playerNumber
        }, payload);
        return
      } else if (tie) {
        //emit win result to server
        socket.emit("client_result", {
          id: data._id,
          result: 'tied'
        }, payload);
        return
      } else {
        //emit move made by user to server
        socket.emit("move_made", payload);
      }
    } else {
      console.log('incorrect input please try again...');
      getInput(data)
    }
  });
}


// emit event to start the game once two players joined the room
socket.on("begin_game", function (data) {
  if (data.playerTwo.id == socketId) {
    myTurn = false;           //to disable player two move
    playerSymbol = 'O'        // to display the symbol
    playerNumber = "second"   //for refrence
    console.log(`Game started. You are the ${playerNumber} player \n` + "Your symbol 'O' \n" + "Waiting For Opponent Move")
  } else {
    console.log(`Game started. You are the ${playerNumber} player \n` + "Your symbol 'X'")
  }
  if (myTurn) {
    printBoard(data.board)    // print current status of board
    getInput(data)            //get command line arguments from users
  }
});


socket.on("move_made", async function (data) {
  printBoard(data.board)  // print current status of board
  //to get input from first player during his turn
  if (playerNumber === "first" && data.playerOneTurn) {
    getInput(data)
  }
  if (playerNumber === "first" && !data.playerOneTurn) {
    console.log("Waiting for opponent Move") //additional information to client
  }

  //to get input from second player during his turn
  if (playerNumber === "second" && !data.playerOneTurn) {
    getInput(data)
  }
  if (playerNumber === "second" && data.playerOneTurn) {
    console.log("Waiting for opponent Move") //additional information to client
  }

})

//To display result  to client
socket.on("result", function (data) {
  printBoard(data.board)
  data.result == "tied" ? console.log(`Game is tied`) : console.log(`Game ${data.result} by ${data.winnerPlayer} player`)
})

instruction()
console.log("Waiting for Opponent")



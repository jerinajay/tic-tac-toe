# tic-tac-toe



## Steps to run the Game

```
git clone https://gitlab.com/jerinajay/tic-tac-toe.git
cd tic-tac-toe
npm i 
```
Open three terminals and type following commands
```
npm run server //1st terminal
npm run client //2nd terminal
npm run client //3rd terminal
``` 
Game instruction will appear in terminal Follow the instruction and play game
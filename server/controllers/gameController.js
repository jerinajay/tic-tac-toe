const gameController = new Object();
const appHelper = require("../helpers/appHelper");
const gameDal = require("../dal/gameDal");
const dot = require('dot-object');


//function triggered to join in a game
gameController.joinGame = async (socket) => {
    try {
        let socketId = socket.id
        let waitingRoom = await gameDal.getPlayersInWaitingRoom() //to check players in waiting room 
        let onlinePlayer = waitingRoom.data.find(p => p.playerOneStatus.disconnected === false)  // to find one online player 
        let room
        //update second player if player one is found
        if (onlinePlayer && waitingRoom.status) {
            let id = onlinePlayer._id
            room = await gameDal.updateGame(id, { 'playerTwo.id': socketId, opponent: true });
        }
        //create first player if no second player is found
        if (!onlinePlayer) {
            room = await gameDal.createGame({ 'playerOne.id': socketId });
        }
        return appHelper.apiResponse(200, true, "Created", room.data);
    } catch (error) {
        console.log("error", error);
        return appHelper.apiResponse(500, false, "Update failed due to internal server error ");
    }
};


//function triggered to update if client is disconnected
gameController.updateGame = async (id,) => {
    try {
        let data = { disconnected: true }
        let updateGame = await gameDal.updateGame(id, data);
        if (!updateGame.status) {
            return appHelper.apiResponse(400, false, updateGame.message, "something went wrong");
        }
        return appHelper.apiResponse(200, true, "Created", updateGame.data);
    } catch (error) {
        console.log("error", error);
        return appHelper.apiResponse(500, false, "Update failed due to internal server error ");
    }
};


//function triggered to update the moves from client
gameController.updateMove = async (data) => {
    try {
        let id = data._id
        let obj = {
            playerOneTurn: data.playerOneTurn,
            board: data.board
        }
        let payload = tgt = dot.dot(obj);
        let updateGame = await gameDal.updateGame(id, payload);
        if (!updateGame.status) {
            return appHelper.apiResponse(400, false, updateGame.message, "something went wrong");
        }
        return appHelper.apiResponse(200, true, "Created", updateGame.data);
    } catch (error) {
        console.log("error", error);
        return appHelper.apiResponse(500, false, "Update failed due to internal server error ");
    }
};



//function triggered to update results in DB
gameController.updateResult = async (data, latestData) => {
    try {
        let id = data.id
        let obj = {
            result: data.result,
            matchStatus:'completed',
            winnerPlayer: data.winnerPlayer,
            board: latestData && latestData.board ? latestData.board : undefined
        }
        obj = JSON.parse(JSON.stringify(obj))
        let payload = tgt = dot.dot(obj);
        let updateGame = await gameDal.updateGame(id, payload);
        if (!updateGame.status) {
            return appHelper.apiResponse(400, false, updateGame.message, "something went wrong");
        }
        return appHelper.apiResponse(200, true, "Created", updateGame.data);
    } catch (error) {
        console.log("error", error);
        return appHelper.apiResponse(500, false, "Update failed due to internal server error ");
    }
};



module.exports = gameController;

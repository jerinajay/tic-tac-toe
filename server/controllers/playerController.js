const playerController = new Object();
const appHelper = require("../helpers/appHelper");
const playerDal = require("../dal/playerDal");


//To log the player is connected
playerController.createPlayer = async (id) => {
    try {
        let createPlayer = await playerDal.createPlayer({ playerId: id });
        if (!createPlayer.status) {
            return appHelper.apiResponse(400, false, createPlayer.message, "something went wrong");
        }
        return appHelper.apiResponse(200, true, "Created", createPlayer.data);
    } catch (error) {
        console.log("error", error);
        return appHelper.apiResponse(500, false, "Update failed due to internal server error ");
    }
};


//To log the player is disconnected
playerController.updatePlayer = async (id, ) => {
    try {
        let data = { disconnected: true }
        let updatePlayer = await playerDal.updatePlayer(id ,data );
        if (!updatePlayer.status) {
            return appHelper.apiResponse(400, false, updatePlayer.message, "something went wrong");
        }
        return appHelper.apiResponse(200, true, "Created", updatePlayer.data);
    } catch (error) {
        console.log("error", error);
        return appHelper.apiResponse(500, false, "Update failed due to internal server error ");
    }
};




module.exports = playerController;

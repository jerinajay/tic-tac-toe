// Data access layer(dal) for player collection 

const playerSchema = require("../models/player");
const playerDal = new Object();

// create newly connected player
playerDal.createPlayer = async (data) => {
    try {
        let player = new playerSchema(data);
        let result = await player.save();
        if (result) {
            return { status: true, data: result };
        }
        return { status: false, data: result }
    } catch (error) {
        console.log("Failed to  Create a player Data", error);
        return { status: false, data: error.message ? error.message : error }
    }
}



//update the player
playerDal.updatePlayer = async (id, data) => {
    try {
        let find = {
            "playerId": id
        }
        let options = {
            upsert: false,
            new: true,
            runValidators: true
        }
        let result = await playerSchema.findOneAndUpdate(find, data, options);
        if (result) {
            return { status: true, data: result };
        }
        return { status: false, data: result }
    } catch (error) {
        console.log("Failed to  Create a player Data", error);
        return { status: false, data: error.message ? error.message : error }
    }
}



module.exports = playerDal;
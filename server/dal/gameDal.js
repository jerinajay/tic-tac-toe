// Data access layer for game collection

const gameSchema = require("../models/game");
const gameDal = new Object();


//aggregate query used to link two collections together to retrieve only online players
gameDal.getPlayersInWaitingRoom = async (data) => {
    try {
        let result = await gameSchema.aggregate()
            .match({ opponent: false })
            .lookup({ from: "players", localField: "playerOne.id", foreignField: "playerId", as: "playerOneStatus" })
            .unwind({ path: "$playerOneStatus",  preserveNullAndEmptyArrays: true})
        if (result.length > 0) {
            return { status: true, data: result };
        }
        return { status: false, data: result }
    } catch (error) {
        console.log("Failed to  Create a game Data", error);
        return { status: false, data: error.message ? error.message : error }
    }
}


//to create new game
gameDal.createGame = async (data) => {
    try {
        let game = new gameSchema(data);
        let result = await game.save();
        if (result) {
            return { status: true, data: result };
        }
        return { status: false, data: result }
    } catch (error) {
        console.log("Failed to  Create Game", error);
        return { status: false, data: error.message ? error.message : error }
    }
}


//to update game status
gameDal.updateGame = async (id, data) => {
    try {
        let find = {
            "_id": id
        }
        let options = {
            upsert: false,
            new: true,
            runValidators: true
        }
        let result = await gameSchema.findOneAndUpdate(find, data, options);
        if (result) {
            return { status: true, data: result };
        }
        return { status: false, data: result }
    } catch (error) {
        console.log("Failed to  Create a player Data", error);
        return { status: false, data: error.message ? error.message : error }
    }
}



//to update game results
gameDal.updateResult = async (id, data) => {
    try {
        let find = {
            "_id": id,
            "matchStatus": 'Inprogress'
        }
        let options = {
            upsert: false,
            new: true,
            runValidators: true
        }
        let result = await gameSchema.findOneAndUpdate(find, data, options);
        if (result) {
            return { status: true, data: result };
        }
        return { status: false, data: result }
    } catch (error) {
        console.log("Failed to  Create a player Data", error);
        return { status: false, data: error.message ? error.message : error }
    }
}



module.exports = gameDal;
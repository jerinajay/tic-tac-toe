const express = require("express");
const defaultRoute = express.Router();

//sample api
defaultRoute.route("/").get(async (req, res) => {
  return res.status(200).send({
    status: true,
    message: "Hurray! tic tac toe server is running successfully"
  });
});

module.exports = defaultRoute;
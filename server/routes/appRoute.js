const express = require("express");
const defaultRoute = require("./defaultRoute");
const appRoute = express.Router();

//middleware to  initialize routes
appRoute.initialize = (app) => {
  app.use("/", defaultRoute);
};

module.exports = appRoute;

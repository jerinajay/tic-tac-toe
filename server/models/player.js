const mongoose = require('mongoose')

//model to save player data
const playerSchema = mongoose.Schema({
  playerId: { type: String },
  disconnected: { type: Boolean, default: false },
}, { timestamps: true })

module.exports = mongoose.model('player', playerSchema);
const mongoose = require('mongoose')


//model to save game
const gameSchema = mongoose.Schema({
  opponent: { type: Boolean, default: false },
  playerOneTurn: { type: Boolean, default: true },
  winnerPlayer: { type: String,  enum : ['first','second'] },
  matchStatus: { type: String,default: 'Inprogress', enum : ['Inprogress','completed'] },
  result: { type: String, enum : ['tied','won'] },
  playerOne: {
    id: { type: String },
    symbol: { type: String, default: 'X' },
  },
  playerTwo: {
    id: { type: String },
    symbol: { type: String, default: 'O' },
  },
  board: {
    1: { type: String, default: ' ',enum : ['X','O',' '], },
    2: { type: String, default: ' ',enum : ['X','O',' '], },
    3: { type: String, default: ' ',enum : ['X','O',' '], },
    4: { type: String, default: ' ',enum : ['X','O',' '], },
    5: { type: String, default: ' ',enum : ['X','O',' '], },
    6: { type: String, default: ' ',enum : ['X','O',' '], },
    7: { type: String, default: ' ',enum : ['X','O',' '], },
    8: { type: String, default: ' ',enum : ['X','O',' '], },
    9: { type: String, default: ' ',enum : ['X','O',' '], }
  }
}, { timestamps: true })

module.exports = mongoose.model('game', gameSchema);
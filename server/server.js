const express = require("express");
const http = require("http");
const socketServer = require("socket.io");
const config = require("./config/config");
const dbConnection = require('./db/database');
const appRoute = require("./routes/appRoute");
const morgan = require("morgan");
const cors = require("cors");
const playerController = require("./controllers/playerController")
const gameController = require("./controllers/gameController")

const initializeServer = async () => {
  await dbConnection.establishConnection();
  const app = express();
  const server = http.createServer(app);
  const io = socketServer(server);
  let port = config.app.port ? config.app.port : 5050;
  /* Below function is used to monitor the socket activity weather new client is connected or connected client is in alive or client is disconnected */
  io.sockets.on("connection", async (socket) => {
    let id = socket.id;
    await playerController.createPlayer(id)
    console.log("New Player connected", id);
    socket.on("mousemove", data => {
      data.id = id;
      socket.broadcast.emit("moving", data);
    });
    socket.on("disconnect", async () => {
      console.log("Player dis-connected", id);
      await playerController.updatePlayer(id, { disconnected: true }) // update player disconnected
      socket.broadcast.emit("player_disconnected", id);
    });
  });

  io.on("connection", async function (socket) {
    let game = await gameController.joinGame(socket); //logic to check join room for game

    // Once the socket has an opponent, we can begin the game
    let opponent = game.data.opponent;
    if (opponent) {
      let playerOneId = game.data.playerOne.id
      socket.join(playerOneId)
      io.to(playerOneId).emit('begin_game', game.data);

    }

    // Listens for a move to be made and emits an event to both
    // players after the move is completed
    socket.on("move_made", async function (data) {
      let updateData = await gameController.updateMove(data); //to update game move in DB
      let playerOneId = updateData.data.playerOne.id
      io.to(playerOneId).emit('move_made', updateData.data); //emit move made to the room where payers joined
    });


    // Listens for a result from client emit 
    socket.on("client_result", async function (data, latestData) {
      let updateResult = await gameController.updateResult(data, latestData); // to update game result  in DB
      let playerOneId = updateResult.data.playerOne.id
      io.to(playerOneId).emit('result', updateResult.data); //emit final result to the room 
    });

  });
  app.use(express.json());                              // used to parse the req body content
  app.use(express.urlencoded({ extended: true }));      // middleware to parses incoming requests
  app.use(morgan("dev"));                               // to log API activity
  app.use(cors());                                      // used to allow cors requests
  app.options('*', cors())
  appRoute.initialize(app);                             // middleware to initialize api routes 
  server.listen(port, () => {                           // initializing the server
    console.log(`listening on port ${port}`);
  });

};

initializeServer();
